#! /bin/bash

if [ -z "$1" ]; then
   echo "Please pass a logfile in parameter ..."
   exit 1
fi

set -e

LOG_FILE="$1"
rm -f $LOG_FILE

run() {
    pod=$1
    name=$2
    pod_name=$3

    #echo "$pod"
    echo "--- $name | $pod_name ---"
    cat <<< $pod | oc create -f-
    LOG_CMD="oc logs -f $pod_name"
    while ! $LOG_CMD 2> /dev/null > /dev/null; do
        echo waiting for $pod_name ...
        sleep 2
        [ "$pod_name" == "startup-arg" ] && break # no output for this one
    done

    echo "--- $name" >> $LOG_FILE
    $LOG_CMD >> $LOG_FILE
    echo "---" >> $LOG_FILE
    echo >> $LOG_FILE

    cat <<< $pod | oc delete -f-
}

for cont in $(ls Containerfile*); do
    image_tag=${cont/Containerfile./}
    name=${image_tag}
    pod_name="startup-${name,,}"
    pod="apiVersion: v1
kind: Pod
metadata:
 name: $pod_name
spec:
 containers:
 - image: quay.io/kpouget/startup:$image_tag
   name: test-pod
 restartPolicy: Never
"
    run "$pod" $name $pod_name

    ###

    name=${image_tag}-command

    pod="apiVersion: v1
kind: Pod
metadata:
 name: $pod_name
spec:
 containers:
 - image: quay.io/kpouget/startup:$image_tag
   name: test-pod
   command:
   - echo
   - command
 restartPolicy: Never
"
    run "$pod" $name $pod_name

    ###

    name=${image_tag}-command-args

    pod="apiVersion: v1
kind: Pod
metadata:
 name: $pod_name
spec:
 containers:
 - image: quay.io/kpouget/startup:$image_tag
   name: test-pod
   command:
   - echo
   - command
   args:
   - echo
   - args
 restartPolicy: Never
"
    run "$pod" $name $pod_name

    ###

    name=${image_tag}-args

    pod="apiVersion: v1
kind: Pod
metadata:
 name: $pod_name
spec:
 containers:
 - image: quay.io/kpouget/startup:$image_tag
   name: test-pod
   args:
   - echo
   - args
 restartPolicy: Never
"
    run "$pod" $name $pod_name
done
