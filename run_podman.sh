#! /bin/bash

set -ex

LOG_FILE=podman.log
rm -f $LOG_FILE
echo '####
# CMD =  ["echo", "CMD"]
# ENTRYPOINT = ["echo", "ENTRY"]
# argv = echo argv
###
' >>  $LOG_FILE

for cont in $(ls Containerfile*); do
    name=${cont/Containerfile./}
    echo "--- $name ---"

    podman build -f $cont -t startup:$name > /dev/null
    podman push startup:$name quay.io/kpouget/startup:$name > /dev/null  2> /dev/null

    echo "--- $name" >> $LOG_FILE
    podman run --rm startup:$name >> $LOG_FILE
    echo "---" >> $LOG_FILE
    echo >> $LOG_FILE

    echo "--- $name+argv" >> $LOG_FILE
    podman run --rm startup:$name echo argv >> $LOG_FILE
    echo "---" >> $LOG_FILE
    echo >> $LOG_FILE
done
