# Simple repository to study Container & Kubernetes container startup commands

We studied: a) `ENTRYPOINT` and `CMD` in the image creation
b) `command` and `args` in the Pod description.


1. `run_podman.sh` builds different images and pushes them to `quay.io`

Here is a summary of the images, and what information is stored in the
OCI description:

## startup:CMD

```
CMD ["echo", "CMD"]

"Cmd": ["echo", "CMD" ]
```

## startup:ENTRY-CMD

```
ENTRYPOINT ["echo", "ENTRYPOINT"]
CMD ["echo", "CMD"]

"Entrypoint": ["echo", "ENTRYPOINT"],
"Cmd": ["echo", "CMD"],
```

## startup:ENTRY

```
ENTRYPOINT ["echo", "ENTRY"]

"Entrypoint": [ "echo", "ENTRY" ],
```

2. `run_k8s.sh <logfile>` all the combinations of `ENTRYPOINT`, `CMD`,
`command` and `args` with the current cluster.

3. Conclusions of the investigations:


Container directives:
```
CMD =  ["echo", "CMD"]
ENTRYPOINT = ["echo", "ENTRY"]
podman CLI args = echo argv
###
```

Pod directives:
```
command = ["echo", "command"]
arg = ["echo", "args"]
```

Syntax:
```
<directives populated> = <directives executed>
```

```
Podman
------

CMD+argv = argv
ENTRY+argv = ENTRY+argv
ENTRY+CMD = ENTRY+CMD
ENTRY+CMD+argv = ENTRY+argv

K8s/OCP
---

CMD+command = command
CMD+command+args = command+args
CMD+args = args

ENTRY+command = command
ENTRY+command+args = command+args
ENTRY+args = ENTRY+args

ENTRY+CMD = ENTRY+CMD
ENTRY+CMD+command = command
ENTRY+CMD+command+args = command+args

ENTRY+CMD+command+args = command+args
ENTRY+CMD+args = ENTRY+args
```
